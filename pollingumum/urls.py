from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.general_polling_form, name='gen-poll')
]