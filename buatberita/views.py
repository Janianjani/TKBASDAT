from django.shortcuts import render
from .forms import News_Form
import datetime
# Create your views here.
response = {}
def post_new(request):
	html = 'buatberita/add_news.html'
	response['news_form'] = News_Form
	return render(request, html, response)

def new_article(request):
	html = 'buatberita/post_edit.html'
	return render(request, html, response)

def create_news_form(request):
	if (request.method == "POST"):
		userlogin = request.session['username']
		judul = request.POST['judul']
		url = request.POST['url']
		topik = request.POST['topik']
		jmlkata = request.POST['jumlahkata']
		tag = request.POST['tag']
		response['judul'] = judul
		response['url'] = url
		response['topik'] = topik
		response['jumlahkata'] = jmlkata
		response['tag'] = tag
		cursor.execute('''INSERT INTO BERITA(url,judul,topik,jumlah_kata) VALUES(%s, %s, %s, %s) ''', [url, judul, topik,jmlkata])
		cursor.execute('''INSERT INTO TAG(url,tag) VALUES(%s, %s) ''', [url, tag])
		html = 'buatberita/result.html'
		return render(request, html, response)
