from django import forms

class News_Form(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    number_attrs = {
        'class': 'form-control',
        'min': 0
    }
    judul = forms.CharField(label='Judul', required=True, max_length=50, widget=forms.TextInput(attrs))
    url = forms.URLField(label='Url', required=True, max_length=50, widget=forms.URLInput(attrs))
    topik = forms.CharField(label='Topik', required=True, max_length=50, widget=forms.TextInput(attrs))
    jumlahkata = forms.IntegerField(label='Jumlah Kata',required=True, widget=forms.NumberInput(number_attrs))
    tag = forms.CharField(label='Tag', required=True, max_length=50, widget=forms.TextInput(attrs))