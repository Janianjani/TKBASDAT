from django.conf.urls import url
from . import views

urlpatterns = [
    #url(r'^$', views.post_list, name='post_list'),
    #url(r'^post/(?P<pk>\d+)/$', views.post_detail, name='post_detail'),
    url(r'^$', views.post_new, name='post_new'),
    url(r'^new/', views.new_article, name='new_article'),
    url(r'^result/', views.create_news_form, name='news_form')
]