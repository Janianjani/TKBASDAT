from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.choosePolling, name='add-polling'),
    url(r'^pollingberita/', views.newspolling_form, name='polling-berita')
]